const mongoose = require('mongoose');
let date = new Date().toJSON();

module.exports = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    uid: {
        type: Number
    },
    vendor: {
        type: String
    },
    date: {
        type: Date,
        default: date
    },
    status: {
        type: Boolean,
        default: 1
    }
});
