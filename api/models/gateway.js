const mongoose = require('mongoose');
const deviceModel = require('./device');
const uniqueValidator = require('mongoose-unique-validator');

const gateway = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    serial_number: {
        type: String,
        required: [true, 'serial number is required'],
        unique: [true, 'serial number should be unique']
    },
    name: {
        type: String
    },
    ipv4_address: {
        type: String,
        required: [true, 'ip address is required'],
        unique: [true, 'ip address should be unique'],
        validate: {
            validator: function (v) {
                return /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/.test(v);
            },
            message: props => `${props.value} is not a valid ip address!`
        },
    },
    status: {
        type: Boolean,
        default: 1
    },
    device: [
        deviceModel
    ]
});

gateway.plugin(uniqueValidator);

module.exports = mongoose.model('gateway', gateway);