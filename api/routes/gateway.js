const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Gateway = require('../models/gateway')

router.get('/', (req, res, next) => {

    const where = {};

    if (req.query.name) {
        where.name = { $regex: '.*' + req.query.name + '.*' }
    }

    if (req.query.status) {
        where.status = req.query.status
    }

    if (req.query.serial_number) {
        where.serial_number = req.query.serial_number
    }

    Gateway.find(where)
        .exec()
        .then(
            data => {
                if (data.length >= 0) {
                    res.status(200).json(data);
                } else {
                    res.status(404).json({
                        msg: "No data found!",
                        statusCode: 404
                    })
                }
            }
        )
        .catch(
            (err) => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    statusCode: 500
                });
            }
        )
});

router.post('/', (req, res, next) => {

    const gateway = new Gateway({
        _id: new mongoose.Types.ObjectId(),
        serial_number: req.body.serial_number,
        name: req.body.name,
        ipv4_address: req.body.ipv4_address,
        status: req.body.status,
        device: req.body.device,
    })

    gateway.save()
        .then(
            () => {
                res.status(201).json({
                    msg: "Gateway created successfully!",
                    statusCode: 201
                });
            }
        )
        .catch(
            (err) => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    statusCode: 500
                })
            }
        )
})

router.patch('/:id', (req, res, next) => {
    const id = req.params.id;
    const updateData = {
        serial_number: req.body.serial_number,
        name: req.body.name,
        ipv4_address: req.body.ipv4_address,
        device: req.body.device
    };

    Gateway.updateOne(
        { _id: id },
        { $set: updateData },
        { runValidators: true }
    )
        .exec()
        .then(
            () => {
                res.status(200).json({
                    msg: "Gateway updated successfully!",
                    statusCode: 200
                });
            }
        )
        .catch(
            (err) => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    statusCode: 500
                })
            }
        )
})

router.put('/status/:id', (req, res, next) => {
    const id = req.params.id;

    Gateway.updateOne(
        { _id: id },
        { status: req.body.status }
    )
        .exec()
        .then(
            () => {
                res.status(200).json({
                    msg: "Gateway status updated successfully!",
                    statusCode: 200
                });
            }
        )
        .catch(
            (err) => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    statusCode: 500
                })
            }
        )
})

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    Gateway.remove({ _id: id })
        .exec()
        .then(
            () => {
                res.status(200).json({
                    msg: "Gateway deleted successfully!",
                    statusCode: 200
                });
            }
        )
        .catch(
            (err) => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    statusCode: 500
                })
            }
        )
})

module.exports = router;