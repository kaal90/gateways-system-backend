const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const gatewayRoutes = require('./api/routes/gateway');

mongoose.set("strictQuery", false);

mongoose.connect('mongodb+srv://' + process.env.MONGO_USER + ':' + process.env.MONGO_PW + '@kaal90.cbqgast.mongodb.net/' + process.env.MONGO_DB + '?retryWrites=true&w=majority')
    .then(() => console.log('Mongo Atlas Connected!'))
    .catch(e => console.log(e));

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
})

//db requests
app.use('/gateway', gatewayRoutes);

app.use((req, res, next) => {
    const err = new Error('No Route Found!');
    err.status = 404;
    next(err);
})

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        err: {
            msg: err.message
        }
    })
});

module.exports = app;