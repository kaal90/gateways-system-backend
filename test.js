const expect = require('chai').expect;
const request = require('supertest');
const mongoose = require('mongoose');
const Mockgoose = require('mockgoose').Mockgoose

const app = require('./app.js');
const Gateway = require("./api/models/gateway.js") 

const mockgoose = new Mockgoose(mongoose);

before(function (done) {
    this.timeout(3000);
    setTimeout(done, 2000);
});

after(async () => {
    try {
        await Gateway.deleteOne({ serial_number: "TEST123" });
    } catch (err) {
        console.error(err);
    }
});

describe('POST /gateway', () => {
    it("Create new gateway, Valid", (done) => {
        request(app)
            .post("/gateway")
            .send({
                serial_number: "TEST123",
                name: "TESTNAME",
                ipv4_address: "222.111.333.443",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(201)
            .then((res) => {
                expect(body).to.contain.property('_id');
                expect(body).to.contain.property('serial_number');
                expect(body).to.contain.property('name');
                expect(body).to.contain.property('ipv4_address');
                expect(body).to.contain.property('status');
                expect(body).to.contain.property('device');
                done();
            })
            .catch((err) => done(err));
    });

    it("Create new gateway, Invalid, Serial number required", (done) => {
        request(app)
            .post("/gateway")
            .send({
                name: "TESTNAME",
                ipv4_address: "222.111.333.444",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(500)
            .then((res) => {
                expect(res.body.message).to.be.eql("serial_number: ValidatorError: serial number is required");
                done();
            })
            .catch((err) => done(err));
      });

      it("Create new gateway, Invalid, Serial number unique", (done) => {
        request(app)
            .post("/gateway")
            .send({
                serial_number: "TEST123",
                name: "TESTNAME",
                ipv4_address: "222.111.333.445",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            },
            {
                serial_number: "TEST123",
                name: "TESTNAME",
                ipv4_address: "222.111.333.446",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(500)
            .then((res) => {
                expect(res.body.message).to.be.eql("serial_number: ValidatorError: Error, expected `serial_number` to be unique. Value: `TEST123`");
                done();
            })
            .catch((err) => done(err));
      });
      it("Create new gateway, Invalid, IP Address required", (done) => {
        request(app)
            .post("/gateway")
            .send({
                serial_number: "TEST1234",
                name: "TESTNAME",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(500)
            .then((res) => {
                expect(res.body.message).to.be.eql("ipv4_address: ValidatorError: ip address is required");
                done();
            })
            .catch((err) => done(err));
      });
      it("Create new gateway, Invalid, IP Address unique", (done) => {
        request(app)
            .post("/gateway")
            .send({
                serial_number: "TEST8234",
                name: "TESTNAME",
                ipv4_address: "222.111.333.449",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            },
            {
                serial_number: "TEST9234",
                name: "TESTNAME",
                ipv4_address: "222.111.333.449",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(500)
            .then((res) => {
                expect(res.body.message).to.be.eql("ipv4_address: ValidatorError: Error, expected `ipv4_address` to be unique. Value: `222.111.333.449`");
                done();
            })
            .catch((err) => done(err));
      });
      it("Create new gateway, Invalid, IP Address unique", (done) => {
        request(app)
            .post("/gateway")
            .send({
                serial_number: "TEST823466",
                name: "TESTNAME",
                ipv4_address: "invalid.test.ip.111",
                status: true,
                device: {
                    uid: 1,
                    vendor: "TESTVENDOR",
                    status: true
                }
            })
            .expect(500)
            .then((res) => {
                expect(res.body.message).to.be.eql("ipv4_address: invalid.test.ip.111 is not a valid ip address");
                done();
            })
            .catch((err) => done(err));
      });
})